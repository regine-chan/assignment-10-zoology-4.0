﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5
{
    public abstract class Horse : Animal
    {
        public string Name { get; set; }

        public Horse(int Height, int Weight, string Name) : base (Height, Weight) 
        {
            this.Name = Name;
        }

        public virtual void Neigh()
        {
            Console.WriteLine("The horse neighed");
        }
    }
}

