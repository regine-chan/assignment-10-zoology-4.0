﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5
{
    public class Monkey : Animal
    {
        public Monkey(int Height, int Weight) : base(Height, Weight)
        {
        }

        public void PickLice()
        {
            Console.WriteLine("The monkey is picking lice from it's own head.");
        }
    }
}
