﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Assignment_5
{
    public class Bird : Animal
    {
        public Bird(int Height, int Weight) : base(Height, Weight)
        {
        }

        public void Chip()
        {
            Console.WriteLine("The bird chips");
        }
    }
}
