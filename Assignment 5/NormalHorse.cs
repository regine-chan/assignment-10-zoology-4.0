﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5
{
    class NormalHorse : Horse, IDrink
    {
        public string OwnerName { get; set; }

        public NormalHorse(string OwnerName, int Height, int Weight, string Name) : base(Height, Weight, Name)
        {
            this.OwnerName = OwnerName;
        }

        public void Move()
        {
            Console.WriteLine("The horse is moving");
        }

        public void Drink()
        {
            Console.WriteLine("The normal horse sips a fine white whipped wine");
        }
    }
}
