﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Assignment_5
{
    public class IslandHorse : Horse, IEat, IDrink
    {
        public Saddle HorseSaddle { get; set; }

        public IslandHorse(int Height, int Weight, string Name) : base(Height, Weight, Name)
        {

        }

        override public void Neigh()
        {
            Console.WriteLine("This horse is apparently mute");
        }

        public void Drink()
        {
            Console.WriteLine("The IslandHorse drinks, SKÅL");
        }

        public void Eat()
        {
            Console.WriteLine("It shall be a tremendous feast!");
        }
    }
}
