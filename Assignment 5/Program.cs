﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace Assignment_5
{
    class Program
    {
        static void Main(string[] args)
        {

            // Initialize program state

            int ProgramState = 1;

            Random rnd = new Random();

            // Pre set list of animals
            List<Animal> animals = new List<Animal>();
            animals.Add(new Alligator(50, 300));
            animals.Add(new Monkey(70, 50));
            animals.Add(new Bird(10, 3));
            animals.Add(new IslandHorse(180, 700, "Horsey"));
            animals.Add(new NormalHorse("Kent", 220, 850, "Horsey2"));
            animals.Add(new Cat(40, 12));
            animals.Add(new Alligator(60, 240));
            animals.Add(new Cat(50, 20));

            // Random print statement with lambda expression
            ProbablyUselessPrintStatement(animals);

            while (ProgramState == 1)
            {
                try
                {
                    SayHello();
                    // User chooses to view or enter
                    int ViewOrEnter = -1;

                    ViewOrEnter = fetchIntegerInput();


                    if (ViewOrEnter == 0)
                    {
                        // User chooses wether to get animals by height or weight
                        SayChoseCriteriaParam();

                        int HeightOrWeight = fetchIntegerInput();
                        if (HeightOrWeight == 0)
                        {
                            SayChoseDirection();
                            // Check if user wants to filter lower or higher than (height)
                            int LowerOrHigher = fetchIntegerInput();
                            
                            if(LowerOrHigher == 0)
                            {
                                // Lower than
                                SayChoseVolume();
                                SayResult(FilterHeightBelow(fetchIntegerInput(), animals));
                            }
                            else if(LowerOrHigher == 1)
                            {
                                // Higher than
                                SayChoseVolume();
                                SayResult(FilterHeightAbove(fetchIntegerInput(), animals));
                            }
                        }
                        else if(HeightOrWeight == 1)
                        {
                            SayChoseDirection();
                            // Check if user wants to filter lower or higher than (Volume)
                            int LowerOrHigher = fetchIntegerInput();

                            if (LowerOrHigher == 0)
                            {
                                // Lower than
                                SayChoseVolume();
                                SayResult(FilterWeightBelow(fetchIntegerInput(), animals));
                            }
                            else if (LowerOrHigher == 1)
                            {
                                // Higher than
                                SayChoseVolume();
                                SayResult(FilterWeightAbove(fetchIntegerInput(), animals));
                            }
                        }
                    } 
                    else if(ViewOrEnter == 1)
                    {
                        // User may input an animal type they want to create
                        // If correct string is written, then an animal of that type is created with random height and weight
                        Console.WriteLine("Enter which animal you want to enter: ");
                        Console.WriteLine("Options are: [alligator, bird, cat, monkey, island horse, normal horse]");

                        string ChosenAnimal = Console.ReadLine();

                        switch (ChosenAnimal)
                        {
                            case "alligator":
                                Animal alligator = new Alligator(rnd.Next(20, 70), rnd.Next(200, 400));
                                Console.WriteLine("You added: \n" + alligator.ToString());
                                animals.Add(alligator);
                                break;
                            case "bird":
                                Animal bird = new Bird(rnd.Next(5, 25), rnd.Next(1, 25));
                                Console.WriteLine("You added: \n" + bird.ToString());
                                animals.Add(bird);
                                break;
                            case "cat":
                                Animal cat = new Cat(rnd.Next(5, 30), rnd.Next(5, 25));
                                Console.WriteLine("You added: \n" + cat.ToString());
                                animals.Add(cat);
                                break;
                            case "monkey":
                                Animal monkey = new Monkey(rnd.Next(20, 70), rnd.Next(5, 80));
                                Console.WriteLine("You added: \n" + monkey.ToString());
                                animals.Add(monkey);
                                break;
                            case "island horse":
                                Animal islandHorse = new IslandHorse(rnd.Next(50, 250), rnd.Next(50, 1000), "Horsey");
                                Console.WriteLine("You added: \n" + islandHorse.ToString());
                                animals.Add(islandHorse);
                                break;
                            case "normal horse":
                                Animal normalHorse = new NormalHorse("MEME", rnd.Next(50, 250), rnd.Next(50, 1000), "Horsey");
                                Console.WriteLine("You added: \n" + normalHorse.ToString());
                                animals.Add(normalHorse);
                                break;
                            default:
                                Console.WriteLine("No such animal, please try again");
                                break;

                        }
                            
                    }

                    // Prompts the user if they want to continue program
                    Console.WriteLine("Do you want to continue? Yes[1] | No[0]");
                    ProgramState = fetchIntegerInput();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Probably failed to parse");
                }

                
                
                
            }
        }

        public static void SayHello()
        {
            Console.WriteLine("Welcome to this program, here you can create animals and filter them based on height and weight.");
            Console.WriteLine("Do you want to look at the animals[0] or enter a new animal[1]");

        }

        public static void SayChoseCriteriaParam()
        {
            Console.WriteLine("Do you want to get a sub list by height[0] or weight[1]? ");
        }

        public static void SayChoseDirection()
        {
            Console.WriteLine("Do you want to filter lower[0] than or higher[1] than?");
        }

        public static void SayChoseVolume()
        {
            Console.WriteLine("Enter the number you want to filter above/below");
        }

        // Fetches an integer input
        public static int fetchIntegerInput()
        {
            return int.Parse(Console.ReadLine());
        }

        // Prints a collection of animals
        public static void SayResult(IEnumerable<Animal> collection)
        {
            foreach (Animal animal in collection){
                Console.WriteLine(animal.ToString());
            }
        }

        // Filters animals below the height user entered
        public static IEnumerable<Animal> FilterHeightBelow(int Volume, List<Animal> animals)
        {
            return from animal in animals
                   where animal.Height < Volume
                   orderby animal.Height descending
                   select animal;
        }

        // Filters animals above the height user entered
        public static IEnumerable<Animal> FilterHeightAbove(int Volume, List<Animal> animals)
        {
            return from animal in animals
                   where animal.Height > Volume
                   orderby animal.Height descending
                   select animal;
        }

        // FIlters animals below the weight entered
        public static IEnumerable<Animal> FilterWeightBelow(int Volume, List<Animal> animals)
        {
            return from animal in animals
                   where animal.Weight < Volume
                   orderby animal.Height descending
                   select animal;
        }

        // Filters animals above the height entered
        public static IEnumerable<Animal> FilterWeightAbove(int Volume, List<Animal> animals)
        {
            return from animal in animals
                   where animal.Weight > Volume
                   orderby animal.Height descending
                   select animal;
        }

        // Prints all animals above 7 in height
        public static void ProbablyUselessPrintStatement(List<Animal> animals)
        {
            SayResult(animals.FindAll(animal => (animal.Height < 20)).ToList());
        }
    }
}
