﻿using System;
using System.Collections.Generic;


namespace Assignment_5
{
    public class Animal
    {
        public int Height { get; set; }

        public int Weight { get; set; }

        public Animal(int Height, int Weight)
        {
            this.Height = Height;
            this.Weight = Weight;
        }

        public void Jump()
        {
            Console.WriteLine("The animal jumped");
        }

        // Prints the animals attributes instead
        public override string ToString()
        {
            return $"Type: {base.ToString().Substring(13)}, Height: {this.Height}, Weight: {this.Height}";
        }
    }
}
