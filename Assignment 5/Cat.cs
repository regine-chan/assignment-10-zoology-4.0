﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Assignment_5
{
    public class Cat : Animal
    {
        public Cat(int Height, int Weight) : base(Height, Weight)
        {
        }

        public void Purr()
        {
            Console.WriteLine("The cat purrs");
        }
    }
}
