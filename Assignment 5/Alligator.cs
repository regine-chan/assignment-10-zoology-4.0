﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5
{
    public class Alligator : Animal
    {
        public Alligator(int Height, int Weight) : base(Height, Weight)
        {
        }

        public void Bite()
        {
            Console.WriteLine("The alligator bit");
        }
    }
}
